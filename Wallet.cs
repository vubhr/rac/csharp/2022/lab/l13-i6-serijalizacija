﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Serijalizacija {
  public class Wallet {
    private List<CryptoCurrency> currencies;

    public Wallet() {
      currencies = new List<CryptoCurrency>();
    }

    public void AddCurrency(CryptoCurrency currency) {
      // ...
    }

    public IEnumerable<CryptoCurrency> GetCurrencies() {
      return
        from c in currencies
        orderby c.Name
        select c;
    }

    public void Serialize() {
      currencies.Add(new CryptoCurrency("bla"));
      string currenciesInJson = JsonSerializer.Serialize(currencies);

      MessageBox.Show(currenciesInJson);
    }

    public void Deserialize() {

    }

  }
}
